# Apache HTTPD Scenario

This scenario will assess your ability to manage Apache HTTPD.

## Setup

## Create a Docker ID account
In order to execute this scenario, you will need a Docker ID.
Go [here](https://docs.docker.com/docker-id/) for instructions to create a _free_ Docker ID.
Follow the instructions to setup your Docker ID.

Go to the [Docker Playground](http://play-with-docker.com) and confirm that you are not a robot. 
In the left-hand menu, click on "ADD NEW INSTANCE"  
This will start a Docker instance (node) and log you in as the root user. Feel free to play around.  
When you are ready, run the command below to startup a server..

`docker run --rm -it dtr.it.vt.edu/rquintin/scenario-httpd:latest`

You should see a bunch of output that ends in something like this:

```
********************************************
**  To browse the site, use curl:         **
**  curl http://localhost/web/index.html  **
********************************************
root@0b028e215eea:/# 
```

## Scenario

You are now logged into a server as the root user.


